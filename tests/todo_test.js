test = require('selenium-webdriver/testing'),
webdriver = require('selenium-webdriver');

test.describe('Create a task', function () {
    test.it('should work', function () {
        this.timeout(15000);
        var driver = new webdriver.Builder().
            withCapabilities(webdriver.Capabilities.chrome()).
            build();
        driver.get('http://localhost:8000');
        driver.findElement(webdriver.By.linkText("Login")).click();

        driver.findElement(webdriver.By.name('email')).sendKeys('matsinet@gmail.com');
        driver.findElement(webdriver.By.name('password')).sendKeys('password');
        driver.findElement(webdriver.By.className("btn btn-primary")).click();

        driver.findElement(webdriver.By.id('task-name')).sendKeys('Mocha task for STL Full Stack Meetup');
        driver.findElement(webdriver.By.className("btn btn-default")).click();

        driver.findElement(webdriver.By.className("btn btn-danger")).click();

        driver.findElement(webdriver.By.linkText("Matt Thomas")).click();
        driver.findElement(webdriver.By.linkText("Logout")).click();

        driver.quit();
    });
});
