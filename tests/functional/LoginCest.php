<?php


class LoginCest
{

    private $name;
    private $email;
    private $password;

    public function _before(FunctionalTester $I)
    {
        $this->name = "John Doe";
        $this->email = date('Ymdhis') . "@functional.test";
        $this->password = "password";
    }

    public function _after(FunctionalTester $I)
    {
    }

    // tests
    public function register(FunctionalTester $I)
    {
        $I->wantTo('Register a new user ' . $this->email);
        $I->amOnPage('/');
        $I->see("Your Application's Landing Page.");

        $I->click('Register');
    }

    public function login(FunctionalTester $I)
    {
    }
}
