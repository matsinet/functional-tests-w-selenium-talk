<?php

class TaskListCest
{

    private $name;
    private $email;
    private $password;
    private $task;

    public function __construct()
    {
        $this->name = "John Doe";
        $this->email = date('Ymdhis') . "@functional.test";
        $this->password = "password";
        $this->task = "Take out trash on " . date('d-m-Y');
    }

    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function register(AcceptanceTester $I)
    {
        $I->wantTo('Register a new user ' . $this->email);
        $I->amOnPage('/');
        $I->see("Your Application's Landing Page.");

        $I->click('Register');

        $I->fillField('name', $this->name);
        $I->fillField('email', $this->email);
        $I->fillField('password', $this->password);
        $I->fillField('password_confirmation', $this->password);

        $I->click('button[type=submit]');
        $I->see($this->name, 'a.dropdown-toggle');
    }

    /**
     * @param AcceptanceTester $I
     *
     * @depends register
     */
    public function logout(AcceptanceTester $I)
    {
        $I->wantTo('Log out user');

        $I->seeInCurrentUrl('tasks');

        $I->click($this->name);
        $I->click('Logout');
        $I->see("Your Application's Landing Page.");
    }

    /**
     * @param AcceptanceTester $I
     *
     * @depends logout
     */
    public function login(AcceptanceTester $I)
    {
        $I->wantTo('Log in user');

        $I->click('Login');
        $I->see('Forgot Your Password?');

        $I->fillField('email', $this->email);
        $I->fillField('password', $this->password);
        $I->click( 'button[type=submit]');

        $I->see('New Task', '.panel-heading');
        $I->seeInCurrentUrl('tasks');
    }

    /**
     * @param AcceptanceTester $I
     *
     * @depends login
     */
    // public function AddTask(AcceptanceTester $I)
    // {
    //     $I->wantTo('Add a task');
    //
    //     $I->fillField('#task-name', $this->task);
    //     $I->click('Add Task');
    //
    //     $I->see($this->task);
    // }
}
