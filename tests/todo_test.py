from selenium import webdriver

driver = webdriver.Chrome()

driver.get('http://localhost:8000')

driver.find_element_by_link_text('Login').click()
driver.find_element_by_name('email').send_keys("matsinet@gmail.com")
driver.find_element_by_name('password').send_keys("password")
driver.find_element_by_class_name('btn-primary').click()

driver.find_element_by_id('task-name').send_keys('Mocha task for STL Full Stack Meetup')
driver.find_element_by_class_name('btn-default').click()

driver.find_element_by_class_name('btn-danger').click()

driver.find_element_by_link_text('Matt Thomas').click()
driver.find_element_by_link_text('Logout').click()

driver.close()